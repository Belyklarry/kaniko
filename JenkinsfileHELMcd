pipeline {
  agent {
    kubernetes {
      yaml """
kind: Pod
metadata:
  namespace: jenkins
spec:
  containers:
  - name: cloudgeeks-ci
    image: python:slim
    command:
    - cat
    tty: true
  - name: cloudgeeks-cd
    image: python:slim
    command:
    - cat
    tty: true
  - name: kaniko
    image: gcr.io/kaniko-project/executor:debug
    imagePullPolicy: Always
    command:
    - sleep
    args:
    - 9999999
    volumeMounts:
    - name: jenkins-docker-cfg
      mountPath: /kaniko/.docker
  volumes:
  - name: jenkins-docker-cfg
    projected:
      sources:
      - secret:
          name: gitlab-credentials
          items:
          - key: .dockerconfigjson
            path: config.json
"""
    }
  }
  stages {
    stage('Build with Kaniko') {
      steps {
        script {
          container('cloudgeeks-ci') {
            withCredentials([string(credentialsId: 'REPO_CREDS_ID', variable: 'REPO_PRIVATE_TOKEN')]) {
              sh '''
                set -e
                echo "Login to CI Repo..."
                echo ${BUILD_NUMBER}
                apt update -y && apt install -y git
                git clone https://oauth2:${REPO_PRIVATE_TOKEN}@gitlab.com/quickbooks2018/kaniko.git
                ls
              '''
            }
          }
          container(name: 'kaniko', shell: '/busybox/sh') {
            sh '''
              set -e
              ls
              pwd
              echo build number is "$BUILD_NUMBER"
              ls
              cd kaniko
              ls
              /kaniko/executor --context `pwd` --dockerfile=Dockerfile  --destination registry.gitlab.com/quickbooks2018/kaniko:${BUILD_NUMBER}
            '''
          }
        }
      }
    }
    stage('Deploy With Helm') {
      steps {
        script {
          container('cloudgeeks-cd') {
            withCredentials([string(credentialsId: 'HELM_REPO_CREDS_ID', variable: 'HELM_REPO_PRIVATE_TOKEN')]) {
              sh '''
                echo "Deploy with Helm..."
                echo "Helm Installation"
                apt update -y && apt install -y curl git
                curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
                chmod 700 get_helm.sh
                ./get_helm.sh
                echo " Build Number is ${BUILD_NUMBER}"
                ls -ltrh
                cd kaniko
                ls -ltrh
                git clone https://oauth2:${HELM_REPO_PRIVATE_TOKEN}@gitlab.com/quickbooks2018/helm-argocd.git
                cd helm-argocd
                helm upgrade --install hello -f hello/values.yaml --set image.repository=registry.gitlab.com/quickbooks2018/kaniko --set image.tag="${BUILD_NUMBER}" --namespace hello hello/ --atomic --timeout 1m25s --cleanup-on-fail
              '''
            }
          }
        }
      }
    }
  }
}